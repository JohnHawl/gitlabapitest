//
//  gitlabapitestApp.swift
//  gitlabapitest
//
//  Created by John Hawley on 5/22/23.
//

import SwiftUI

@main
struct gitlabapitestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
